import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product = new Product();
    product.name = createProductDto.name;
    product.price = parseFloat(createProductDto.price);
    if (createProductDto.image && createProductDto.image !== '') {
      product.image = createProductDto.image;
    }
    product.type = JSON.parse(createProductDto.type);
    return this.productsRepository.save(product);
  }

  findAll() {
    return this.productsRepository.find({ relations: { type: true } });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    product.name = updateProductDto.name;
    product.price = parseFloat(updateProductDto.price);
    if (updateProductDto.image && updateProductDto.image !== '') {
      product.image = updateProductDto.image;
    }
    product.type = JSON.parse(updateProductDto.type);
    await this.productsRepository.save(product);
    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    await this.productsRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
